const ERROR_ALERT_TEXT_GET_CORRECT_ANSWER = "An unexpected error has occured. The game might need to be restarted and your current progress will be lost. We are really sorry for this inconvenience.";
const ERROR_ALERT_TEXT_GAME_CONTROLLER = "An unexpected error has occured. Contact game owner for further informations.";
const BUTTON_CLASS_DEFAULT = 'btn-default';
const BUTTON_CLASS_CORRECT = 'btn-success';
const BUTTON_CLASS_WRONG = 'btn-danger';
const numberOfQuestions = 10;
var app = null;
var init = 0;
var stage = 0;
var gameInProgress = true;
var questionNumber = 0;
var questionsArray = [];
var countdown = null;
var points = 0;
var pointsPerAnswer = 10;

const view0 = '<div class="center-content">' +
				'<h2>Wybierz poziom trudności</h2>' +
				'<div class="button-wrapper">' +
					'<button type="button" class="btn btn-default btn-lg btn-block option option1">Łatwy</button>' +
					'<button type="button" class="btn btn-default btn-lg btn-block option option2">Średni</button>' +
					'<button type="button" class="btn btn-default btn-lg btn-block option option3">Trudny</button>' +
				'</div>' +
			'</div>';
const view1 = '<h2>Zaczynamy za:</h2>' +
			'<p id="countdown"></p>';
const view2 = '<div class="row">' +
				'<div class="question col-12">' +
					'<h1 id="questionContent">Question content</h2>' +
					'<h2 id="countdown">XX</h3>' +
				'</div>' +
			'</div>' +
			'<div class="row answers buttons-wrapper">' +
				'<button type="button" class="btn btn-default btn-lg btn-block col-12 col-md-6 offset-md-1 option option1">Answer 1</button>' +
				'<button type="button" class="btn btn-default btn-lg btn-block col-12 col-md-6 offset-md-1 option option2">Answer 2</button>' +
				'<button type="button" class="btn btn-default btn-lg btn-block col-12 col-md-6 offset-md-1 option option3">Answer 3</button>' +
				'<button type="button" class="btn btn-default btn-lg btn-block col-12 col-md-6 offset-md-1 option option4">Answer 4</button>' +
			'</div>';
const view3 = '<h2>Uzyskana ilość punktów</h2>' +
			'<h3 id="result-points"></h3>' +
			'<img id="reward" class="rounded mx-auto d-block" src="reward.jpg" />';

function initGame() {
	app = document.getElementById("app");
	if (app && init == 0) {
		init = 1;
		initView(0);
	} else if (!app && init == 0) {
		var reinitializeGame = setInterval(function() {
			app = document.getElementById("app");
			if (app && init == 0) {
				init = 1;
				initView(0);
				clearInterval(reinitializeGame);
				initGame();
			}
		}, 1000);
	}

	if (app && init == 1) {
		gameController(stage);
	}
}

document.addEventListener("DOMContentLoaded", function() {
	initGame();
});

function getView(stage) {
	return eval('view' + stage);
}

function initView(stage) {
	app.innerHTML = getView(stage);
}

function chooseDifficulty() {
	var difficultyButtons = document.getElementsByClassName("option");
	for (var i = 0; i < difficultyButtons.length; i++) {
		difficultyButtons[i].addEventListener('click', function() {
			var currentButtonsClass = this.className.match("option1") || this.className.match("option2") || this.className.match("option3");
			var difficultyLevel = String(currentButtonsClass).substr(6, 7);
			getQuestionsForCurrentDifficulty(difficultyLevel);
			stage = 1;
			gameController(stage);
		});
	}
}

function getQuestionsForCurrentDifficulty(difficultyLevel) {
	var request = new XMLHttpRequest();

	request.onreadystatechange = function() {
		if (request.readyState === 4) {
			if (request.status === 200) {
				processData(request.responseText);
			} else {
				gameInProgress = false;
				alert(ERROR_ALERT_TEXT_GET_CORRECT_ANSWER);
			} 
		}
	}

	request.open('POST', 'poziomTrudnosci' + difficultyLevel + '.csv', true);
	request.send();
}

function processData(questionsData) {
	var splitRecords = questionsData.replace(/\n/g, ";end&line;").split(";end&line;");
	var currentQuestionData = [];

	for (var i = 0; i < splitRecords.length; i++) {
		currentQuestionData = splitRecords[i].split(';');
		questionsArray.push(currentQuestionData);
	}
}

function startCountdown(time, initStage) {
	var countdownField = document.getElementById("countdown");
	var executed = false;
	var timer = null;
	if (timer == null && executed == false) {
		timer = time;
		executed = true;
		countdownField.innerHTML = timer;
	}
	countdown = setInterval(function() {
		timer--;
		countdownField.innerHTML = timer;
		if (timer <= 0 && executed) {
			clearInterval(countdown);
			if (initStage)
				stage = initStage;
			gameController(stage);
		}
	}, 1000);
}

function stopCountdown() {
	clearInterval(countdown);
}

function setQuestions(questionNumber) {
	var questionContent = document.getElementById("questionContent");
	var answerButtons = document.getElementsByClassName("option");

	questionContent.innerHTML = questionsArray[questionNumber][1];
	for (var i = 0; i < answerButtons.length; i++) {
		answerButtons[i].textContent = questionsArray[questionNumber][i+2];
		answerButtons[i].addEventListener('click', function() {
			var currentButtonsClass = this.className.match("option1") || this.className.match("option2") || this.className.match("option3") || this.className.match("option4");
			var chosenAnswer = String(currentButtonsClass).substr(6, 7);
			for (var j = 0; j < answerButtons.length; j++) {
				answerButtons[j].disabled = true;
			}
			answerValidator(chosenAnswer, questionsArray[questionNumber][6], currentButtonsClass);
		});
	}
}

function markAnswer(currentButtonsClass, isCorrect) {
	currentButtonsClass[0].classList.remove(BUTTON_CLASS_DEFAULT);
	if (isCorrect) {
		currentButtonsClass[0].classList.add(BUTTON_CLASS_CORRECT);
	} else {
		currentButtonsClass[0].classList.add(BUTTON_CLASS_WRONG);
	}
}

function answerValidator(userAnswer, correctAnswer, currentButtonsClass) {
	var localUserAnswer = parseInt(userAnswer);
	var localCorrectAnswer = parseInt(correctAnswer);
	var clickedButtonsClass = document.getElementsByClassName(currentButtonsClass);
	if (localUserAnswer == localCorrectAnswer) {
		// Perform some fancy style transition on chosen answer
		points += pointsPerAnswer;
		markAnswer(clickedButtonsClass, true);
	} else {
		// Perform some fancy style transition on chosen answer
		markAnswer(clickedButtonsClass, false)
	}

	stopCountdown();
	if (questionNumber < numberOfQuestions) {
		startCountdown(3, null);
	} else {
		gameController(stage);
	}
}

function displayResult() {
	document.getElementById("result-points").innerHTML = points;
}

function gameController(stage) {
	if (init == 1 && stage == 0 || init == 1 && gameInProgress == true) {
		if (stage == 0) {
			initView(stage);
			chooseDifficulty();
		}
		if (stage == 1) {
			initView(stage);
			startCountdown(3, 2);
		}
		if (stage == 2) {
			initView(stage);
			if (questionNumber >= 0 && questionNumber < numberOfQuestions) {
				setQuestions(questionNumber);
				startCountdown(30, null);
				questionNumber++;
			} else {
				stage = 3;
				gameController(stage);
			}
		}
		if (stage == 3) {
			initView(stage);
			displayResult();
		}
	} else {
		app.innerHTML = '<button id="restart">Restart game</button>';
		document.getElementById("restart").addEventListener('click', function() {
			window.location.reload();
		});
		alert(ERROR_ALERT_TEXT_GAME_CONTROLLER);
	}
}
